/**
 * Gulp Packages
 */

// General
const { task, parallel, src, watch, dest, series } = require('gulp')
const del = require('del')
const wiredep = require('wiredep').stream
const gulpif = require('gulp-if')
const useref = require('gulp-useref')
const plumber = require('gulp-plumber')
const flatten = require('gulp-flatten')
const rename = require('gulp-rename')
const imagemin = require('gulp-imagemin')
const browserSync = require('browser-sync').create()

// Scripts
const uglify = require('gulp-uglify')

// Styles
const sass = require('gulp-dart-sass')
const prefix = require('gulp-autoprefixer')
const minify = require('gulp-cssnano')
const cssimport = require('gulp-cssimport')

// Pug
const pug = require('gulp-pug-i18n')
const marked = require('marked')

// Buil tasks to run and watch
const buildTypes = ['pug', 'styles', 'scripts', 'static', 'fonts']

// Paths to project folders
const paths = {
  input: 'src/**',
  output: 'dist/',
  scripts: {
    watch: 'src/js/**',
    input: 'dist/js/**',
    output: 'dist/js/',
  },
  styles: {
    watch: 'src/sass/**',
    input: 'src/sass/*.{scss,sass}',
    output: 'dist/css/',
    wiredep: 'src/sass',
  },
  pug: {
    watch: ['src/pages/**', 'src/locale/*.yml'],
    input: 'src/pages/*.pug',
    output: 'dist/',
    // locales: 'src/locale/*.yml',
    // filename: '{{lang}}/{{basename}}.html',
    locales: 'src/locale/fr.yml',
    filename: '{{basename}}.html',
    wiredep: 'src/pages',
  },
  static: {
    input: ['src/static/**', 'src/static/.*'],
    output: 'dist/',
  },
  fonts: {
    input: 'bower_components/font-awesome/fonts/*',
    output: 'dist/fonts/',
  },
}

/**
 * Gulp Tasks
 */
// Compile Pug files into output folder
task('build:pug', () =>
  src(paths.pug.input)
    .pipe(plumber())
    // Combine JS tags
    .pipe(
      useref({
        searchPath: ['.', 'src'],
      })
    )
    .pipe(
      gulpif(
        '**/*.pug',
        pug({
          i18n: {
            locales: paths.pug.locales,
            filename: paths.pug.filename,
          },
          data: {
            // Markdown
            md: (t) =>
              marked(t)
                // Add &nbsp; before punctuation
                .replace(/ (\!|\?|:)/g, '&nbsp;$1'),
          },
        })
      )
    )
    //.pipe(gulpif('**/*.html', gulp.dest(paths.pug.output))
    .pipe(dest(paths.pug.output))
    .pipe(browserSync.stream())
)

// Minify, and concatenate scripts
task('build:scripts', () =>
  src(paths.scripts.input)
    .pipe(plumber())
    .pipe(uglify())
    .pipe(dest(paths.scripts.output))
    .pipe(browserSync.stream())
)

// Process and minify Sass files
task('build:styles', () =>
  src(paths.styles.input)
    .pipe(plumber())
    .pipe(sass())
    .pipe(flatten())
    .pipe(
      prefix({
        cascade: true,
        remove: true,
      })
    )
    .pipe(cssimport({}))
    .pipe(dest(paths.styles.output))
    .pipe(rename({ suffix: '.min' }))
    .pipe(
      minify({
        discardComments: {
          removeAll: true,
        },
      })
    )
    .pipe(dest(paths.styles.output))
    .pipe(browserSync.stream())
)

// Inject bower components
task(
  'wiredep',
  series(
    // Styles
    () =>
      src(paths.styles.wiredep + '/**')
        .pipe(wiredep())
        .pipe(dest(paths.styles.wiredep)),
    // Pages
    () =>
      src(paths.pug.wiredep + '/**')
        .pipe(
          wiredep({
            ignorePath: /^(\.\.\/)*\.\./,
          })
        )
        .pipe(dest(paths.pug.wiredep))
  )
)

// Copy static files into output folder
task('build:static', () =>
  src(paths.static.input)
    .pipe(plumber())
    // Optimize images
    .pipe(imagemin())
    .pipe(dest(paths.static.output))
    .pipe(browserSync.stream())
)

// Copy font files into output folder
task('build:fonts', () =>
  src(paths.fonts.input)
    .pipe(plumber())
    .pipe(dest(paths.fonts.output))
    .pipe(browserSync.stream())
)

// Remove pre-existing content from output folder
task('clean:dist', () => {
  const delDist = () => del(paths.output)
  let tries = 5
  // Try to delete folder multiple times
  return delDist().catch((e) => {
    if (tries != 0) {
      tries--
      return new Promise((resolve) => {
        setTimeout(resolve, 500)
      }).then(delDist)
    }
    throw e
  })
})

/**
 * Task Runners
 */

// Build project
task('build', series(...buildTypes.map((t) => `build:${t}`)))

// Clean and Build project
task('default', series('clean:dist', 'build'))

// Watch files
task('watch', (cb) => {
  buildTypes.forEach((type) => {
    const path = paths[type].watch || paths[type].input
    watch(path, series(`build:${type}`))
  })
  cb()
})

// Serve files with Browsersync
task('browserSync', (cb) => {
  //gulp.watch('bower.json', ['wiredep'])
  browserSync.init({
    server: paths.output,
  })
  cb()
})

task('start', series('default', 'watch', 'browserSync'))
