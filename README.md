# lonestone.consulting Website

This is the source of [https://lonestone.consulting]()

This project uses:

- `gulp` to build the static website
- `bower` to manage the web packages
- `pug` for the templates
- `sass` for CSS preprocessing

## Dev

### Install

Install yarn and bower dependencies:

    yarn install

Run Gulp with server and livereload:

    yarn start

Go to [http://localhost:3000/fr/](http://localhost:3000/fr/)

### Add a Bower package

Install package (eg: jquery):

    yarn run bower-install --save jquery

Update bower script tags in pug files:

    yarn run wiredep

Script tags of bower packages will be inserted between `<!-- bower:js -->` and `<!-- endbower -->` comment tags.

## Prod

Install dependencies and build static website:

    yarn install && yarn run build

Put `./dist` folder behind an HTTP server (Nginx, Apache2...).
