$(document).ready(function () {
    "use strict";

    // Jobs Lightbox
    $('.jobs .photos').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        mainClass: 'mfp-fade'
    });

    // WOW JS
    if ($(window).scrollTop() < 100) {
        new WOW().init();
    }

    // Smooth Scrool
    smoothScroll.init();

    // Sticky Nav
    $("#main-nav").sticky({
        topSpacing: 0
    });

    var hashId = location.hash && location.hash.slice(1);
    if (hashId) {
        setTimeout(function() {
            var elementToShow = $("#" + hashId);
            if(elementToShow.length) {
                document.body.parentElement.scrollTop = elementToShow.offset().top - 130
            }
        }, 100);
    }

    // Offers
    $(".offer").each(function(i, offer) {
        var id = offer.id;
        offer = $(offer);
        var content = offer.find('.content');
        var link = offer.find('.title a');
        // Show only offer with id in URL's hash
        if (hashId !== id) {
            content.hide();
        }
        // Open offer on click
        link.click(function(e) {
            if (e.shiftKey || e.ctrlKey) return;
            e.preventDefault();
            changeHash(id);
            // Toggle content
            content.slideToggle(200);
        });
    });

    // Drift
    initDrift()

    // Map
    if (typeof(mapboxgl) === 'undefined' || $('#map').length === 0) return;

    var position = [-1.559824335234123, 47.2062016677096];
    mapboxgl.accessToken = 'pk.eyJ1IjoibG9uZXN0b25lIiwiYSI6ImNpcjNvdWltbTAwMjdpNG1jOGo5aWhjaHkifQ.M1PHd0GZhSFe7s7jJNlF5g';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/lonestone/cixbzdlfj00iu2pnwy7coe00i',
        interactive: false,
        center: position,
        zoom: 14
    });
    var $marker = $('.map-marker').remove();
    var $popup = $('.map-popup').remove();
    var popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false,
        offset: [0, -40]
    })
        .setDOMContent($popup[0]);
    var marker = new mapboxgl.Marker($marker[0], {offset: [-18, -36]})
        .setLngLat(position)
        .setPopup(popup)
        .addTo(map)
        .togglePopup();

});

function initDrift() {
    var t = window.driftt = window.drift = window.driftt || [];
    if (!t.init) {
        if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
        t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
        t.factory = function(e) {
        return function() {
            var n = Array.prototype.slice.call(arguments);
            return n.unshift(e), t.push(n), t;
        };
        }, t.methods.forEach(function(e) {
        t[e] = t.factory(e);
        }), t.load = function(t) {
        var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
        o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
        var i = document.getElementsByTagName("script")[0];
        i.parentNode.insertBefore(o, i);
        };
    }
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('5g9d52g8678z');
}

// https://stackoverflow.com/a/1489802/2104725
function changeHash(hash) {
    hash = hash.replace(/^#/, '');
    var fx, node = $('#' + hash);
    if (node.length) {
        node.attr('id', '');
        fx = $('<div></div>')
            .css({
                position: 'absolute',
                visibility: 'hidden',
                top: $(document).scrollTop() + 'px'
            })
            .attr('id', hash)
            .appendTo(document.body);
    }
    document.location.hash = hash;
    if (node.length) {
        fx.remove();
        node.attr('id', hash);
    }
}
