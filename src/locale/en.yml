lang: en

header:
  brand: Lonestone

navbar:
  services: Services
  tools: Tools
  culture: Culture
  team: Team
  portfolio: References
  jobs: Jobs
  contact: Contact

readmore: Read More

page404:
  title: Lonestone - Page not found
  notfound: "I can't find this page, Sorry!"
  consulting: 'We build, develop and manage web applications and video games. [Need our expertise?](./#services)'
  cityinvaders: 'Have you heard of our game [City Invaders](https://cityinvaders.game/) on smartphones and PC?'
  home: "Lost? [Go back to the homepage](./), it's really cool, you'll see!"
  copyright: '&copy; Illustrations are from our video game [City Invaders](https://cityinvaders.game/)'

home:
  title: Lonestone - Dev and design agency
  description: We help you succeed in your webapps and mobile apps. And we make video games 😁

intro:
  text: We **design**, **develop** and **improve** <br />your **webapps** and **mobile apps**
  expertises:
    web: Web
    mobile: Mobile
    backend: Backend
    bots: Bots
    games: Games

team:
  headTitle: Lonestone's team
  title: Our Team
  jobs:
    dev: Software Engineer
    sysadmin: Sysadmin
    manager: Project Manager
    artist2d: 2D Artist
    artist3d: 3D Artist
    uxui: UX/UI Designer
    business: Business Consultant
    hr: HR Manager

services:
  title: We are at your side to...

  audit:
    name: Audit and improve
    icon: search
    text:
      "We aim at being always at the forefront in terms of ergonomics and performance, elegance of the code
      and working methods. Lonestone's team will be happy to bring all its expertise to your company."

  design:
    name: Design
    icon: sitemap
    text:
      'We understand the constraints your projects may have: chosing modern and practical technologies, optimize
      user experience, maximize compatibility with targeted devices, analyze performances.'

  dev:
    name: Build
    icon: code
    text:
      'We develop your applications hand in hand with your team, from our office in Nantes, France.
      We are especially efficient in the development of APIs using Node, webapps using React or Vue.js
      and mobile apps using React Native.'

  formation:
    name: Train your team
    icon: book
    text:
      "Teaching and sharing are part of Lonestone's culture. We will be proud to share our knowledge with your
      employees and introduce them to new technologies to help them be more efficient, master solid tools, patterns and
      methods."

  sysadmin:
    name: Administrate your servers
    icon: terminal
    text:
      'We build and manage hosting on dedicated servers, AWS, Google Cloud and Azure for high traffic websites,
      making sure to optimize costs, performances, redundancy, replication, scalability, backups and continuous deployment.'

tools:
  title: Our favorite tools

values:
  title: Our values
  cta: Discover our culture

culture:
  headTitle: La culture chez Lonestone
  title: Notre culture
  description: Lonestone est une entreprise libérée, donnant le pouvoir à ses salariés de faire évoluer l'organisation et de s'épanouir. Notre culture est forte et bienveillante.

  intro: >
    Depuis la création de Lonestone, nous avons souhaité centrer l'entreprise sur la qualité des relations humaines, la confiance et l'agilité. Nous ne sommes pas des machines, nous avons besoin d'une **philosophie humaniste** pour travailler ensemble.


    Nous croyons qu'il est possible de **mieux réussir et de s'épanouir**, collectivement et personnellement, en appliquant les principes décrits ci-dessous.

  framework: >
    Pour cela, nous avons un cadre composé de :

    - **Principes** fondamentaux et immuables.

    - **Valeurs** : choix moraux et culturels décidées collectivement, régissant nos actions.

    - **Missions** : raison d'être de l'entreprise et de chacune de ses parties.

    - **Gouvernance** : outils et méthodes d'organisation

  quote: Lonestone est une **entreprise libérée**, donnant le pouvoir à ses salariés de faire évoluer l'organisation et de s'épanouir.

  principles:
    title: Nos Principes
    list:
      - name: Transparence radicale
        icon: search
        text: La transparence permet de prendre de meilleures décisions, d'apporter plus de justice et de rendre chacun plus responsable. Toutes les informations de l'entreprise sont donc accessibles à tous les salariés.

      - name: Honnêteté radicale
        icon: smile-o
        text: Tout l'équipe doit être radicalement honnête, toujours avec bienveillance. N'avoir rien à cacher résout des problèmes, enlève du stress et construit la confiance.

      - name: Confiance
        icon: thumbs-up
        text: La confiance est clé pour toutes les interactions au sein de nos équipes. Nous faisons confiance dans la sincérité et dans les compétences* de chacun.

      - name: Responsabilité
        icon: gears
        text: Chacun·e peut et doit prendre les initiatives qu'il/elle juge bonnes et en prendre la responsabilité, sans oublier de communiquer avec les autres. Nous ne devons pas tout attendre de leaders.

  governance:
    title: Gouvernance
    text: >
      **Concrètement, comment ça se passe ?** L'auto-organisation a besoin d'outils et de méthodes de gouvernance pour fonctionner correctement.


      Chez Lonestone, nous utilisons des concepts de gouvernance empruntés à la sociocratie et à l'[holacratie](https://en.wikipedia.org/wiki/Holacracy), sans nous y restreindre. Notre organisation n'est pas figée, elle est structurée en rôles et en cercles, et évolue en fonction des besoins, par décisions collégiales (décisions par consentement, élections sans candidat).

  values:
    title: Nos Valeurs
    list:
      - name: Penser Long Terme
        icon: eye
        text: 'Nos ambitions sont grandes, notre structure et nos réalisations sont prévues pour évoluer et pour durer. Nous construisons pour le futur, un futur toujours plus passionnant, riche, confortable et équitable. "Quick and dirty" n''est en réalité souvent pas plus rapide, mieux vaut toujours faire bien.'

      - name: Toujours apprendre
        icon: book
        text: "S'améliorer est plus important qu'être le meilleur, surtout dans un environnement aussi changeant que le nôtre. Nous améliorons chaque jour la qualité de nos produits, de nos compétences et de notre organisation. Le progrès nécessite une réflexion personnelle et collective, reconnaissant nos erreurs et questionnant nos habitudes."

      - name: Respect et Bienveillance
        icon: heart
        text: "Nos collègues, clients et partenaires ont souvent des besoins différents des nôtres, nous devons comprendre leurs points de vue et communiquer efficacement. Nous travaillons mieux ensemble avec de l'empathie, et nous créons les meilleurs produits lorsque nous comprenons les gens pour qui on les crée."

      - name: Communiquer et coopérer
        icon: comments
        text: "L'humain est un animal social, nous avons un besoin fondamental de communiquer pour être heureux et efficaces. Nous sommes une famille, une tribu qui travaille en équipe et coopère avec l'extérieur : clients, open source, écoles, etc. Nous n'hésitons pas à demander de l'aide et à aider spontanément les autres."

      - name: Être Autonome et Agile
        icon: paper-plane
        text: "La réussite de notre entreprise est la somme des investissements personnels de chaque collaborateur. Nous prenons chacun la responsabilité de remplir nos objectifs et nous prenons rapidement les dispositions nécessaires pour alerter et s'adapter en cas de besoin."

      - name: Agir avec Ethique
        icon: leaf
        text: "Nous voulons être fiers de nos actions, et pour cela une bonne morale est de mise. Une réussite à l'éthique douteuse ne serait pas vraiment une réussite pour nous. L'éthique est au cœur de nos relations humaines et de nos stratégies commerciales, nous en débattons dès qu'une question éthique se pose."

portfolio:
  title: References

  eklablog:
    headline: Eklablog
    text:
      '**Design**, **development** (PHP, MySQL), **system administration** for 8 years. Eklablog is a blogging
      platform with over 10 million unique visitors per month. It is now owned by Webedia.'

  eatwith:
    headline: Eatwith
    text:
      'Collaboration with Eatwith (formerly VizEat) technical team to **develop** their new **API (Node, PostgreSQL)** as well as
      their new **React webapp**.'

  fvt:
    headline: France Vélo Tourisme
    text:
      '**Technical audit** of the France Vélo Tourisme platform and its servers. We identified and helped
      **solving** bugs, security flaws, performance issues and architecture problems.'

  cityinvaders:
    headline: City Invaders
    text:
      '**Design**, Game design, **development** (**Unity3D, C#, Node, PostgreSQL, React**) and marketing of the
      video game City Invaders on Android, iOS and Steam.'

  imie:
    headline: IMIE
    text: '**Javascript and Git** courses and member of the jury for Software Development programs'

  efrei:
    headline: EFREI
    text: "Web technologies seminar with EFREI's engineering students: **Node backend, API architecture**"

  isep:
    headline: ISEP
    text: 'PL/SQL, Cloud, Cassandra, MongoDB, Neo4j, JDBC **courses**.'

  biborg:
    headline: Biborg
    text:
      'Development of various **web applications, backends and Twitter bots**, cloud hosting (AWS + Google Cloud).
      We used **Node, Typescript, React.js, Vue.js, Express.js, Nightmare / Electron, FFMPEG, Docker, AWS (Amazon EC2, ECS, SES)**'

  startuponly:
    headline: StartupOnly
    text:
      '**Technical audit** of the platform (source code and server), **Dev**: complete rework of the backend, frontend and backoffice.
      We used **NestJs, Vue.js, Nuxt, Typescript, PostgreSQL, Docker, Kubernetes, AWS (S3), Google Cloud**'

  blocinbloc:
    headline: Bloc in Bloc
    text:
      'Migration of an Angular.js (v1) and Meteor application to a **new Angular 2 application** along with
      a **Typescript and Express.js** backend.'

  hexanet:
    headline: Hexanet
    text: 'Reinforcing the development team on a **big e-commerce web application in React / Redux**.'

  gojob:
    headline: Gojob
    text:
      "Development of a back-office ein **React / Redux using Typescript and Ant Design**,
      in collaboration with Gojob's technical team."

  qivivo:
    headline: Qivivo
    text: 'Refactoring of the Qivivo client dashboard in **Vue.js**'

  follow:
    headline: Follow
    text:
      "Développement de la webapp de suivi de patients en **React / Redux et Typescript**
      et de l'app pour tablette (Android et iOS) en React Native."

  garagescore:
    headline: GarageScore
    text: "Renfort d'équipe technique pour développer des **composants Vue.js**"

  neadz:
    headline: Neadz
    text: 'Développement du backend Node.js avec **Nest.js et Typescript**.'

  voodoo:
    headline: Voodoo
    text: "Développement **Node.js et Vue.js** avec interconnexions d'APIs et scraping."

  lucette:
    headline: Lucette
    text: 'Refonte totale du front avec  **Vue.js et Nuxt.js**.'

  foodmeup:
    headline: FoodMeUp
    text: 'Développement **React / Redux avec Typescript**.'

  nokia:
    headline: Nokia Bell Labs
    text: "Développement d'une webapp interactive avec **React, Lottie et Typescript**."

  forcity:
    headline: ForCity
    text: 'Développement **Vue.js et Node.js / Sequelize avec Typescript**.'

jobs:
  title: We're hiring!
  text: |
    You wish to join a company where your input matters, with a love for a job well done and a good spirit?
    Contact us!

    We are always looking for talented programmers and artists to create and work on ambitious and fascinating
    projects.
  cta: See our Jobs Offers
  intro: |
    Lonestone develops video games, webapps and mobile apps using the best current technologies and methods.

    We love digital products, user experience, design and beautiful code. You too? Join us, we're growing!
  whyus:
    title: 😀 Why join us?
    list:
      - 'Our company is liberated: flat organization, collective decisions, absolute transparency'
      - We focus on people and stimulating projects
      - We're ambitious, but we always favor quality over quantity
      - We share strong values and we respect them
  bonus:
    title: ⭐ Bonuses
    list:
      - Relaxed atmosphere in a wonderful office
      - Competitive salary, luncheon voucher, health insurance, welfare plan
      - Flexible schedule, quality equipment, unlimited coffee, tea and fruits
      - Partial remote work
  btnOffers: See all our job offers

jobsOffers:
  title: Our offers
  headTitle: Lonestone is hiring!
  description: 'Offers: Dev Javascript - Designer UX/UI'
  youare: 'You are:'
  bonus: 'Bonus points for:'
  offers:
    - position: Lead Dev
      id: lead-dev
      type: CDI
      description: |
        Vous serez en charge de développements d'applications web et/ou d'APIs avec une petite équipe.

        Vous déciderez de l'architecture et des technologies à utiliser, et vous gérerez le projet en contact quotidien avec le client (depuis nos locaux).
      youare:
        - Expert·e en Javascript (Ecmascript 6+ / Typescript)
        - Habitué·e en Node.js, React, Vue.js ou Angular
        - Habitué·e aux workflows Git et à l'intégration continue
        - Organisé·e et méthodique
        - Autonome et bon·ne communicant·e
        - À l'aise en anglais (et en français bien sûr, sans linter)
      bonus:
        - Analyse technique, estimations et découpage des tâches
        - SCRUM master
        - Veille technologique quotidienne
        - Expérience en formation ou audit

    - position: UX/UI Designer
      id: ux-ui-designer
      type: CDI
      xp: Confirmé·e / Senior
      description: |
        Suite au développement de notre offre UX/UI et à son succès, nous recrutons une deuxième personne pour agrandir l'équipe de Design ! 🎉

        Vous concevrez des webapps, sites web et apps mobiles principalement pour des startups. Vous travaillerez en autonomie, mais vous  serez aussi en collaboration directe avec notre équipe (UI/UX designer, développeurs, business developer) et avec nos clients.
      youare:
        - Expérimenté·e en animation d'ateliers, en conception de livrables et en création de wireframes et prototypes
        - 'Expérimenté·e en création graphique : conception de moodboard, de chartes graphiques et de webdesign'
        - Habitué·e à l'utilisation d'outils de conception comme Sketch, Figma, Adobe XD, InVision (et/ou autres outils de prototypage)
        - Habitué·e à l'approche par composants et au responsive design
        - 'Intéressé·e par la recherche utilisateurs : interviews, études terrains, focus group, études quali/quanti, questionnaires en ligne, tests utilisateurs'
        - 'Autonome pour préparer et animer des ateliers de co-conception avec des clients et/ou utilisateurs : ateliers personas, parcours utilisteurs, tri par carte, ateliers de co-conception wireframe ou graphique'
        - Capable de définir des méthodologies appropriées aux projets en avant-vente et établir des chiffrages en UX et en UI
      bonus:
        - Sensible aux bonnes pratiques d'accessibilité
        - "À l'aise avec des méthodologies de conception rapide : sprint design, lean UX..."
        - Connaissances de l'intégration web (HTML, CSS, javascript) pour évaluer la faisabilité technique
        - Compétences en illustration (c'est la cerise sur le gâteau !)

    - position: Dev frontend React
      id: dev-react
      type: CDI
      xp: Confirmé·e / Senior
      description: |
        Vous développerez des webapps en React avec une petite équipe.

        Vous suivez assidûment les évolutions de React et vous êtes toujours prêt·e à apprendre de nouveaux outils, frameworks et méthodes.
      youare:
        - Très à l'aise en Javascript (Ecmascript 6+ / Typescript), HTML et CSS
        - Expérimenté·e en organisation de composants et de stores (state management)
        - Habitué·e au travail en équipe (workflow Git, agile)
        - Autonome et bon·ne communicant·e
      bonus:
        - Connaissance de Redux, Next.js, hooks, Typescript
        - Utilisation de Invision, Zeplin, Storybook
        - Optimisation, accessibilité

    - position: Dev frontend Vue.js
      id: dev-vue
      type: CDI
      xp: Confirmé·e / Senior
      description: |
        Vous développerez des webapps en Vue.js avec une petite équipe.

        Vous suivez assidûment les évolutions de Vue.js et vous êtes toujours prêt·e à apprendre de nouveaux outils, frameworks et méthodes.
      youare:
        - Très à l'aise en Javascript (Ecmascript 6+ / Typescript), HTML et CSS
        - Expérimenté·e en organisation de composants et de stores (state management)
        - Habitué·e au travail en équipe (workflow Git, agile)
        - Autonome et bon·ne communicant·e
      bonus:
        - Connaissance de Vuex, Nuxt.js, vue-cli
        - Utilisation de Invision, Zeplin, Storybook
        - Optimisation, accessibilité

    - position: Dev React Native
      id: dev-react-native
      type: CDI
      xp: Confirmé·e / Senior
      description: |
        Vous développerez des applications mobiles en React Native avec une petite équipe.

        Vous suivez assidûment les évolutions de React et React Native et vous êtes toujours prêt·e à apprendre de nouveaux outils, frameworks et méthodes.
      youare:
        - Très à l'aise en Javascript (Ecmascript 6+ / Typescript)
        - Expérimenté·e en organisation de composants et de stores (state management)
        - Habitué·e au travail en équipe (workflow Git, agile)
        - Autonome et bon·ne communicant·e
      bonus:
        - Connaissance de Redux, Expo, hooks, Typescript
        - Utilisation de Invision, Zeplin, Storybook
        - Optimisation, UX mobile

    # - position: Dev backend Node.js
    #   id: dev-node
    #   type: CDI
    #   xp: Confirmé·e / Senior
    #   description: |
    #     Vous concevrez et développerez des APIs et bases de données avec Node.js et Nest.js.

    #     Vous suivez assidûment les tendances de l'écosystème et vous êtes toujours prêt·e à apprendre de nouveaux outils, frameworks et méthodes.
    #   youare:
    #     - Très à l'aise en Javascript (Ecmascript 6+ / Typescript)
    #     - Exigeant·e sur les modèles de données relationels
    #     - Sensible aux problématiques de sécurité et de qualité
    #     - Habitué·e au travail en équipe (workflow Git, agile)
    #     - Autonome et bon·ne communicant·e
    #   bonus:
    #     - Connaissance de Nest.js, Typescript, TypeORM / Sequelize
    #     - Connaissance de Docker
    #     - 'Compétences en devop: Gitlab CI, Circle CI, Kubernetes'
    #     - Utilisation de Swagger

    # - position: Dev Unity3D confirmé·e
    #   id: dev-unity
    #   type: CDI
    #   description: |
    #     Vous développerez des jeux vidéo avec l'équipe Studio en utilisant Unity3D / C#.
    #     Vous suivez assidûment les nouveautés de Unity3D et vous êtes toujours prêt·e à apprendre de nouveaux outils et méthodes.
    #   youare:
    #     - Très à l'aise en C#
    #     - Polyvalent, capable de toucher à des shaders, de la UI ou encore de la modélisation 3D procédurale
    #     - Habitué·e au travail en équipe (workflow Git, agile)
    #     - Autonome et bon·ne communicant·e
    #   bonus:
    #     - Programmation réactive (UniRX)
    #     - Développement de shaders
    #     - Optimisation de performances
    #     - Connaissances en FSM et Behaviour Trees
    #     - Sensibilité graphique
    # - position: Chargé·e de Communication
    #   id: communication
    #   type: Stage 6 mois
    #   description: |
    #     Vous aiderez à concevoir et à appliquer une stratégie de communication pour Lonestone ainsi que pour les jeux vidéo et autres produits que nous développons.
    #   youare:
    #     - Irréprochable à l'écrit
    #     - Sensible à la communication visuelle
    #     - Bourré·e d'idées originales pour mieux communiquer
    #     - Méthodique et autonome
    #   bonus:
    #     - Utilisation de Photoshop
    #     - Comptes actifs sur Twitter, Reddit, Twitch, Product Hunt
    # - position: Business Developer
    #   id: biz-dev
    #   type: Stage
    #   description: |
    #     En tant que stagiaire business developer au sein de notre agence, rattaché·e à la Direction commerciale (@Pierre-Luc Lelouch),
    #     vous renforcerez notre équipe de 2 personnes pour développer le business entrant de Lonestone.
    #     * Stage de 4 à 8 semaines
    #     * Possibilité de télétravail partiel
    #     * Pas de gratification financière
    #     * Titres restaurant et prise en charge de 50% des frais de transport
    #   youare:
    #     - Sensibilisé·e aux enjeux business de conquête et de fidélisation
    #     - Sensible au marché numérique, de l'innovation et des startups
    #     - A l'aise avec les outils digitaux et les réseaux sociaux
    #     - Doté·e d'un très bon redactionnel
    #     - Organisé·e et méthodique
    #     - Autonome et bon·ne communicant·e
    #   bonus:
    #     - A l'aise en anglais
    #     - Expérience en marketing automation
    #     - Notions en marketing digital

contact:
  title: Contact Us
