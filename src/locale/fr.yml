lang: fr

header:
  brand: Lonestone

navbar:
  services: Métiers
  tools: Outils
  culture: Culture
  team: Équipe
  portfolio: Références
  jobs: Jobs
  contact: Contact

readmore: En savoir plus

page404:
  title: Lonestone - Page introuvable
  notfound: 'Je ne trouve pas cette page, Désolée !'
  consulting: 'Nous concevons, développons et administrons des applications web et des jeux vidéo. [Besoin de nos services ?](./#services)'
  cityinvaders: 'Connaissez-vous notre jeu [City Invaders](https://cityinvaders.game/) sur smartphones et PC ?'
  home: "Perdu ? [Revenez à l'accueil](./), c'est extrêmement intéressant."
  copyright: '&copy; Les illustrations proviennent de notre jeu vidéo [City Invaders](https://cityinvaders.game/)'

home:
  title: Lonestone - Agence de dev et design à Nantes
  description: On vous aide à réussir vos webapps et apps mobile. Et nous créons des jeux vidéo 😁

intro:
  text: On **design**, **développe** et **améliore** <br />vos **webapps** et **apps mobile**
  expertises:
    web: Web
    mobile: Mobile
    backend: Backend
    bots: Bots
    games: Jeux

team:
  headTitle: L'équipe de Lonestone
  title: Notre Équipe
  jobs:
    dev: Ingénieur Développeur
    sysadmin: Administrateur Système
    manager: Chef de Projet
    artist2d: Artiste 2D
    artist3d: Artiste 3D
    uxui: UX/UI Designer
    business: Consultant Métier
    hr: Responsable RH

services:
  title: Nous vous accompagnons pour...

  audit:
    name: Auditer et améliorer
    icon: search
    text:
      "Nous mettons un point d'honneur à être toujours à la pointe en termes d'ergonomie, de performances,
      d'élégance du code et de méthodes de travail. L'équipe de Lonestone sera heureuse d'apporter toute
      son expertise à votre entreprise."

  design:
    name: Concevoir
    icon: sitemap
    text:
      "Nous comprenons bien les contraintes que peuvent avoir vos projets : faire des choix technologiques
      modernes et pragmatiques, optimiser l'expérience utilisateur, maximiser la compatibilité avec les périphériques
      visés, analyser les performances."

  dev:
    name: Réaliser
    icon: code
    text:
      "Nous développons vos applications en totale harmonie avec votre équipe, depuis nos locaux à Nantes.
      Nous sommes particulièrement efficaces dans le développement d'APIs avec Node, de webapps avec
      React ou Vue.js, et d'apps mobiles avec React Native."

  formation:
    name: Former votre équipe
    icon: book
    text:
      "L'enseignement et le partage font partie de la culture de Lonestone. Nous serons fiers d'ouvrir nos
      connaissances à vos employés pour leur faire découvrir de nouvelles technologies, être plus efficaces et
      maîtriser des outils, patterns et méthodes."

  sysadmin:
    name: Administrer vos serveurs
    icon: terminal
    text:
      "Nous construisons et gérons des hébergements sur serveurs dédiés, AWS, Google Cloud et Azure pour
      des sites à fort trafic, en prenant soin d'optimiser les coûts, les performances, la redondance,
      la réplication, la scalabilité, les backups et le déploiement continu."

tools:
  title: Nos outils de prédilection

values:
  title: Nos valeurs
  cta: Découvrir notre culture

culture:
  headTitle: La culture chez Lonestone
  title: Notre culture
  description: Lonestone est une entreprise libérée, donnant le pouvoir à ses salariés de faire évoluer l'organisation et de s'épanouir. Notre culture est forte et bienveillante.

  intro: >
    Depuis la création de Lonestone, nous avons souhaité centrer l'entreprise sur la qualité des relations humaines, la confiance et l'agilité. Nous ne sommes pas des machines, nous avons besoin d'une **philosophie humaniste** pour travailler ensemble.


    Nous croyons qu'il est possible de **mieux réussir et de s'épanouir**, collectivement et personnellement, en appliquant les principes décrits ci-dessous.

  framework: >
    Pour cela, nous avons un cadre composé de :

    - **Principes** fondamentaux et immuables.

    - **Valeurs** : choix moraux et culturels décidées collectivement, régissant nos actions.

    - **Missions** : raison d'être de l'entreprise et de chacune de ses parties.

    - **Gouvernance** : outils et méthodes d'organisation

  quote: Lonestone est une **entreprise libérée**, donnant le pouvoir à ses salariés de faire évoluer l'organisation et de s'épanouir.

  principles:
    title: Nos Principes
    list:
      - name: Transparence radicale
        icon: search
        text: La transparence permet de prendre de meilleures décisions, d'apporter plus de justice et de rendre chacun plus responsable. Toutes les informations de l'entreprise sont donc accessibles à tous les salariés.

      - name: Honnêteté radicale
        icon: smile-o
        text: Tout l'équipe doit être radicalement honnête, toujours avec bienveillance. N'avoir rien à cacher résout des problèmes, enlève du stress et construit la confiance.

      - name: Confiance
        icon: thumbs-up
        text: La confiance est clé pour toutes les interactions au sein de nos équipes. Nous faisons confiance dans la sincérité et dans les compétences* de chacun.

      - name: Responsabilité
        icon: gears
        text: Chacun·e peut et doit prendre les initiatives qu'il/elle juge bonnes et en prendre la responsabilité, sans oublier de communiquer avec les autres. Nous ne devons pas tout attendre de leaders.

  governance:
    title: Gouvernance
    text: >
      **Concrètement, comment ça se passe ?** L'auto-organisation a besoin d'outils et de méthodes de gouvernance pour fonctionner correctement.


      Chez Lonestone, nous utilisons des concepts de gouvernance empruntés à la sociocratie et à l'[holacratie](https://en.wikipedia.org/wiki/Holacracy), sans nous y restreindre. Notre organisation n'est pas figée, elle est structurée en rôles et en cercles, et évolue en fonction des besoins, par décisions collégiales (décisions par consentement, élections sans candidat).

  values:
    title: Nos Valeurs
    list:
      - name: Penser Long Terme
        icon: eye
        text: 'Nos ambitions sont grandes, notre structure et nos réalisations sont prévues pour évoluer et pour durer. Nous construisons pour le futur, un futur toujours plus passionnant, riche, confortable et équitable. "Quick and dirty" n''est en réalité souvent pas plus rapide, mieux vaut toujours faire bien.'

      - name: Toujours apprendre
        icon: book
        text: "S'améliorer est plus important qu'être le meilleur, surtout dans un environnement aussi changeant que le nôtre. Nous améliorons chaque jour la qualité de nos produits, de nos compétences et de notre organisation. Le progrès nécessite une réflexion personnelle et collective, reconnaissant nos erreurs et questionnant nos habitudes."

      - name: Respect et Bienveillance
        icon: heart
        text: "Nos collègues, clients et partenaires ont souvent des besoins différents des nôtres, nous devons comprendre leurs points de vue et communiquer efficacement. Nous travaillons mieux ensemble avec de l'empathie, et nous créons les meilleurs produits lorsque nous comprenons les gens pour qui on les crée."

      - name: Communiquer et coopérer
        icon: comments
        text: "L'humain est un animal social, nous avons un besoin fondamental de communiquer pour être heureux et efficaces. Nous sommes une famille, une tribu qui travaille en équipe et coopère avec l'extérieur : clients, open source, écoles, etc. Nous n'hésitons pas à demander de l'aide et à aider spontanément les autres."

      - name: Être Autonome et Agile
        icon: paper-plane
        text: "La réussite de notre entreprise est la somme des investissements personnels de chaque collaborateur. Nous prenons chacun la responsabilité de remplir nos objectifs et nous prenons rapidement les dispositions nécessaires pour alerter et s'adapter en cas de besoin."

      - name: Agir avec Ethique
        icon: leaf
        text: "Nous voulons être fiers de nos actions, et pour cela une bonne morale est de mise. Une réussite à l'éthique douteuse ne serait pas vraiment une réussite pour nous. L'éthique est au cœur de nos relations humaines et de nos stratégies commerciales, nous en débattons dès qu'une question éthique se pose."

portfolio:
  title: Références

jobs:
  title: Nous recrutons !
  headTitle: Lonestone recrute !
  description: 'Postes : Dev Javascript - Designer UX/UI'
  text: |
    Vous souhaitez rejoindre une entreprise où votre contribution compte, avec un amour du travail bien fait et
    une ambiance bienveillante ? Contactez-nous !

    Nous sommes toujours à la recherche de talentueu·x·ses développeu·rs·ses et artistes pour créer des projets passionants et
    ambitieux.
  cta: Voir nos Offres d'Emploi
  intro: |
    Lonestone développe des jeux vidéo, des webapps et des apps mobiles avec les meilleures technologies et méthodes actuelles.

    Nous sommes des amoureux des produits numériques, de l’expérience utilisateur, du design et du beau code. Vous aussi ? Rejoignez-nous, on grandit !
  whyus:
    title: 😀 Pourquoi nous rejoindre ?
    list:
      - 'Notre entreprise est libérée : organisation horizontale, décisions collégiales, transparence totale'
      - Nous privilégions l'humain et les projets intéressants
      - Nous avons une ambition débordante, mais nous privilégions toujours la qualité à la quantité
      - Nous partageons des valeurs fortes et nous les respectons
  bonus:
    title: ⭐ Les petits plus
    list:
      - Ambiance décontractée dans de beaux locaux
      - Salaire attractif, tickets resto, activités et formations
      - Horaires libres, bon matériel, café/thé et fruits à volonté
      - Télétravail partiel libre
  btnOffers: Voir toutes nos offres d'emploi

jobsOffers:
  title: Nos offres
  youare: 'Vous êtes :'
  bonus: 'Points bonus pour :'
  offers:
    - position: Lead Dev
      id: lead-dev
      type: CDI
      description: |
        Vous serez en charge de développements d'applications web et/ou d'APIs avec une petite équipe.

        Vous déciderez de l'architecture et des technologies à utiliser, et vous gérerez le projet en contact quotidien avec le client (depuis nos locaux).
      youare:
        - Expert·e en Javascript (Ecmascript 6+ / Typescript)
        - Habitué·e en Node.js, React, Vue.js ou Angular
        - Habitué·e aux workflows Git et à l'intégration continue
        - Organisé·e et méthodique
        - Autonome et bon·ne communicant·e
        - À l'aise en anglais (et en français bien sûr, sans linter)
      bonus:
        - Analyse technique, estimations et découpage des tâches
        - SCRUM master
        - Veille technologique quotidienne
        - Expérience en formation ou audit

    - position: UX/UI Designer
      id: ux-ui-designer
      type: CDI
      xp: Confirmé·e / Senior
      description: |
        Suite au développement de notre offre UX/UI et à son succès, nous recrutons une deuxième personne pour agrandir l'équipe de Design ! 🎉

        Vous concevrez des webapps, sites web et apps mobiles principalement pour des startups. Vous travaillerez en autonomie, mais vous  serez aussi en collaboration directe avec notre équipe (UI/UX designer, développeurs, business developer) et avec nos clients.
      youare:
        - Expérimenté·e en animation d'ateliers, en conception de livrables et en création de wireframes et prototypes
        - 'Expérimenté·e en création graphique : conception de moodboard, de chartes graphiques et de webdesign'
        - Habitué·e à l'utilisation d'outils de conception comme Sketch, Figma, Adobe XD, InVision (et/ou autres outils de prototypage)
        - Habitué·e à l'approche par composants et au responsive design
        - 'Intéressé·e par la recherche utilisateurs : interviews, études terrains, focus group, études quali/quanti, questionnaires en ligne, tests utilisateurs'
        - 'Autonome pour préparer et animer des ateliers de co-conception avec des clients et/ou utilisateurs : ateliers personas, parcours utilisteurs, tri par carte, ateliers de co-conception wireframe ou graphique'
        - Capable de définir des méthodologies appropriées aux projets en avant-vente et établir des chiffrages en UX et en UI
      bonus:
        - Sensible aux bonnes pratiques d'accessibilité
        - "À l'aise avec des méthodologies de conception rapide : sprint design, lean UX..."
        - Connaissances de l'intégration web (HTML, CSS, javascript) pour évaluer la faisabilité technique
        - Compétences en illustration (c'est la cerise sur le gâteau !)

    - position: Dev frontend React
      id: dev-react
      type: CDI
      xp: Confirmé·e / Senior
      description: |
        Vous développerez des webapps en React avec une petite équipe.

        Vous suivez assidûment les évolutions de React et vous êtes toujours prêt·e à apprendre de nouveaux outils, frameworks et méthodes.
      youare:
        - Très à l'aise en Javascript (Ecmascript 6+ / Typescript), HTML et CSS
        - Expérimenté·e en organisation de composants et de stores (state management)
        - Habitué·e au travail en équipe (workflow Git, agile)
        - Autonome et bon·ne communicant·e
      bonus:
        - Connaissance de Redux, Next.js, hooks, Typescript
        - Utilisation de Invision, Zeplin, Storybook
        - Optimisation, accessibilité

    - position: Dev frontend Vue.js
      id: dev-vue
      type: CDI
      xp: Confirmé·e / Senior
      description: |
        Vous développerez des webapps en Vue.js avec une petite équipe.

        Vous suivez assidûment les évolutions de Vue.js et vous êtes toujours prêt·e à apprendre de nouveaux outils, frameworks et méthodes.
      youare:
        - Très à l'aise en Javascript (Ecmascript 6+ / Typescript), HTML et CSS
        - Expérimenté·e en organisation de composants et de stores (state management)
        - Habitué·e au travail en équipe (workflow Git, agile)
        - Autonome et bon·ne communicant·e
      bonus:
        - Connaissance de Vuex, Nuxt.js, vue-cli
        - Utilisation de Invision, Zeplin, Storybook
        - Optimisation, accessibilité

    - position: Dev React Native
      id: dev-react-native
      type: CDI
      xp: Confirmé·e / Senior
      description: |
        Vous développerez des applications mobiles en React Native avec une petite équipe.

        Vous suivez assidûment les évolutions de React et React Native et vous êtes toujours prêt·e à apprendre de nouveaux outils, frameworks et méthodes.
      youare:
        - Très à l'aise en Javascript (Ecmascript 6+ / Typescript)
        - Expérimenté·e en organisation de composants et de stores (state management)
        - Habitué·e au travail en équipe (workflow Git, agile)
        - Autonome et bon·ne communicant·e
      bonus:
        - Connaissance de Redux, Expo, hooks, Typescript
        - Utilisation de Invision, Zeplin, Storybook
        - Optimisation, UX mobile

    # - position: Dev backend Node.js
    #   id: dev-node
    #   type: CDI
    #   xp: Confirmé·e / Senior
    #   description: |
    #     Vous concevrez et développerez des APIs et bases de données avec Node.js et Nest.js.

    #     Vous suivez assidûment les tendances de l'écosystème et vous êtes toujours prêt·e à apprendre de nouveaux outils, frameworks et méthodes.
    #   youare:
    #     - Très à l'aise en Javascript (Ecmascript 6+ / Typescript)
    #     - Exigeant·e sur les modèles de données relationels
    #     - Sensible aux problématiques de sécurité et de qualité
    #     - Habitué·e au travail en équipe (workflow Git, agile)
    #     - Autonome et bon·ne communicant·e
    #   bonus:
    #     - Connaissance de Nest.js, Typescript, TypeORM / Sequelize
    #     - Connaissance de Docker
    #     - 'Compétences en devop: Gitlab CI, Circle CI, Kubernetes'
    #     - Utilisation de Swagger

    # - position: Dev Unity3D confirmé·e
    #   id: dev-unity
    #   type: CDI
    #   description: |
    #     Vous développerez des jeux vidéo avec l'équipe Studio en utilisant Unity3D / C#.
    #     Vous suivez assidûment les nouveautés de Unity3D et vous êtes toujours prêt·e à apprendre de nouveaux outils et méthodes.
    #   youare:
    #     - Très à l'aise en C#
    #     - Polyvalent, capable de toucher à des shaders, de la UI ou encore de la modélisation 3D procédurale
    #     - Habitué·e au travail en équipe (workflow Git, agile)
    #     - Autonome et bon·ne communicant·e
    #   bonus:
    #     - Programmation réactive (UniRX)
    #     - Développement de shaders
    #     - Optimisation de performances
    #     - Connaissances en FSM et Behaviour Trees
    #     - Sensibilité graphique
    # - position: Chargé·e de Communication
    #   id: communication
    #   type: Stage 6 mois
    #   description: |
    #     Vous aiderez à concevoir et à appliquer une stratégie de communication pour Lonestone ainsi que pour les jeux vidéo et autres produits que nous développons.
    #   youare:
    #     - Irréprochable à l'écrit
    #     - Sensible à la communication visuelle
    #     - Bourré·e d'idées originales pour mieux communiquer
    #     - Méthodique et autonome
    #   bonus:
    #     - Utilisation de Photoshop
    #     - Comptes actifs sur Twitter, Reddit, Twitch, Product Hunt
    # - position: Business Developer
    #   id: biz-dev
    #   type: Stage
    #   description: |
    #     En tant que stagiaire business developer au sein de notre agence, rattaché·e à la Direction commerciale (@Pierre-Luc Lelouch),
    #     vous renforcerez notre équipe de 2 personnes pour développer le business entrant de Lonestone.
    #     * Stage de 4 à 8 semaines
    #     * Possibilité de télétravail partiel
    #     * Pas de gratification financière
    #     * Titres restaurant et prise en charge de 50% des frais de transport
    #   youare:
    #     - Sensibilisé·e aux enjeux business de conquête et de fidélisation
    #     - Sensible au marché numérique, de l'innovation et des startups
    #     - A l'aise avec les outils digitaux et les réseaux sociaux
    #     - Doté·e d'un très bon redactionnel
    #     - Organisé·e et méthodique
    #     - Autonome et bon·ne communicant·e
    #   bonus:
    #     - A l'aise en anglais
    #     - Expérience en marketing automation
    #     - Notions en marketing digital

contact:
  title: Contactez-nous
