FROM node:8-alpine
EXPOSE 3000

RUN apk --update --no-cache add  \
		git \
    automake \
		alpine-sdk  \
		nasm  \
		autoconf  \
		build-base \
		zlib \
		zlib-dev \
		libpng \
		libpng-dev\
		libwebp \
		libwebp-dev \
		libjpeg-turbo \
		libjpeg-turbo-dev

WORKDIR /usr/src/app

# Install app dependencies
COPY package.json .
COPY bower.json .
COPY yarn.lock .

RUN yarn install

# Bundle app source
COPY . .
RUN yarn run build

# Serve with files on http
# Warning: .htaccess won't work
RUN yarn global add http-server
CMD http-server -p 3000 dist
